(local lume (require :polywell.lib.lume))
(local editor (require :polywell))

(local mock {})
(setmetatable mock {:__index (fn [] (fn [] mock))})
(set love.graphics mock)

(local seed (tonumber (or (os.getenv "POLYWELL_FUZZ_SEED") (os.time))))
(print "Seeding with" seed)
(math.randomseed seed)
(love.math.setRandomSeed seed)

(fn vals [t]
  (let [rtn {}]
    (each [_ v (pairs t)] (table.insert rtn v))
    rtn))

(fn try [f]
  (var traceback nil)
  (let [(ok err) (xpcall f (fn [e]
                             (print e)
                             (print (debug.traceback))))]
    (when (not ok)
      (editor.debug)
      (print (.. "POLYWELL_FUZZ_SEED=" seed " make fuzz"))
      (love.event.quit))))

(fn random-text []
  (var input "")
  (for [_ 1 (love.math.random 10)]
    (set input (.. input (string.char (+ (love.math.random (- 127 32)) 32))))
    (when (= (love.math.random 5) 1)
      (set input (.. input " "))))
  input)

(local commands-by-mode {})
(local binding-for {})
(fn commands-for [mode]
  (or (. commands-by-mode mode)
      (let [commands (lume.merge mode.map mode.ctrl mode.alt mode.ctrl-alt)
            parent (and mode.parent (. (editor.debug "modes") mode.parent))]
        (tset commands-by-mode mode
              (lume.concat (vals commands)
                           (and parent (commands-for parent))))
        (each [k command (pairs commands)]
          (var key k)
          (if (lume.find mode.ctrl command) (set key (.. "ctrl-" key))
              (lume.find mode.alt command) (set key (.. "alt-" key))
              (lume.find mode.ctrl-alt command) (set key (.. "ctrl-alt-" key)))
          (tset binding-for command key))
        (each [i c (lume.ripairs (. commands-by-mode mode))]
          (when (= (type c) "table") ; prefix maps
            (lume.extend (. commands-by-mode mode)
                         (table.remove (. commands-by-mode mode) i))))
        (. commands-by-mode mode))))

(local files {})
(local fs {:read (fn [path] (. files path))
           :write (fn [path contents] (tset files path contents))
           :type (fn [path] (if (. files path) :file nil))})

(fn fuzz [n]
  (editor.init "fuzzo" "edit" ["hey" ""] fs)
  (for [i 1 n]
    (when (and (= (# (editor.buffer-names)) 1)
               (~= editor.current-buffer-name "minibuffer"))
      (print "opening newfile")
      (editor.open "newfile"))

    (let [mode (. (editor.debug "modes") (editor.current-mode-name))
          command (assert (lume.randomchoice (commands-for mode))
                          (.. "no command in " mode.name))]
      (print (.. "run " (. binding-for command) " in mode " mode.name))
      (try (lume.fn editor.handlers.internal___wrap command))
      (when love.keyreleased (love.keyreleased))
      (when (= (love.math.random 5) 1)
        (try (lume.fn editor.handlers.textinput (random-text)))))))

(fuzz (tonumber (or (os.getenv "POLYWELL_FUZZ_COUNT") 256)))
(love.event.quit)
