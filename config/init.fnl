(local editor (require :polywell))

(editor.add-mode {:name "base"
                  :ctrl {"x" {:ctrl {"f" editor.cmd.find-file}
                              :map {"1" editor.cmd.split
                                    "2" (partial editor.cmd.split "vertical")
                                    "3" (partial editor.cmd.split "horizontal")
                                    "4" (partial editor.cmd.split "triple")
                                    "b" editor.cmd.switch-buffer
                                    "k" editor.cmd.close
                                    "o" editor.cmd.focus-next}}
                         "q" editor.quit
                         "pageup" editor.cmd.next-buffer
                         "pagedown" editor.cmd.prev-buffer}
                  :alt {"x" editor.cmd.execute
                        "return" editor.cmd.toggle-fullscreen}
                  :ctrl-alt {"b" editor.cmd.switch-buffer}})

(editor.add-mode (require :config.edit-mode))
(editor.add-mode (require :config.fennel-mode))
(editor.add-mode (require :config.lua-mode))
(editor.add-mode (require :config.clojure-mode))
(editor.add-mode (require :config.nrepl-mode))
(editor.add-mode (require :config.console)) ; for lua code
(editor.add-mode (require :config.repl)) ; for fennel code

(editor.init "*repl*" "repl" ["This is the repl. Enter code to run." ">> "])
