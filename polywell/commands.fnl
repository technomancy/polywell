(local lume (require :polywell.lib.lume))
(local completion (require :polywell.completion))
(local utf8 (require :polywell.lib.utf8))
(local editor (require :polywell.old))
(local state (require :polywell.state))
(local frontend (require :polywell.frontend))

;;; buffers, files, and windows

(fn find-file []
  (let [callback (fn [input]
                   (when (= :file (state.fs.type input))
                     (editor.open input)))

        completer          ; offer live feedback as you type
        (fn [input]
          (let [parts (lume.split input "/")
                file-part (table.remove parts)
                dir-part (table.concat parts "/")
                completions (completion.for input (state.fs.ls dir-part))
                ;; different types should be distinct
                decorate (λ [path]
                           (if (= :file (state.fs.type path))
                               path
                               (.. path "/")))]
            ;; if it's not a table/string, we shouldn't see it
            (lume.sort (lume.filter (lume.map completions decorate))
                       (λ [s1 s2] (< (# s1) (# s2))))))]
    (editor.read-line "Open: " callback {:completer completer})))

(fn close [confirm]
  (if (and state.b.needs_save (not confirm))
      (editor.echo "Unsaved changes!")
      (editor.kill-buffer)))

(fn switch-buffer []
  (let [last-buffer (or (editor.last-buffer) "*repl*")
        callback (fn [b]
                   (when (and b (lume.find (editor.buffer-names) b))
                     (let [b (if (= b "") last-buffer b)]
                       (set state.last-buffer state.b)
                       (editor.change-buffer b))))
        completer (fn [input]
                    (completion.for input (editor.buffer-names)))]
    (editor.read-line (.. "Switch to buffer (default: " last-buffer "): ")
                      callback {:completer completer})))

(fn next-buffer [n]
  (let [current (- (lume.find state.buffers state.b) 1)
        target (+ (% (+ current (or n 1)) (# state.buffers)) 1)
        target-path (. (editor.buffer-names) target)]
    (set state.last-buffer state.b)
    (editor.change-buffer target-path)))

(fn focus-next [n]
  (when state.splits
    (set state.last-buffer state.b)
    (let [current (or (editor.current-split) 1)
          to (+ current (or n 1))]
      (set state.b (. state.splits (if (> to (# state.splits)) 1 to) 2)))))

(fn split [style]
  (let [(w h) (frontend.get_wh)
        hw (/ w 2) hh (/ h 2)
        second (or state.last-buffer state.b)]
    (set state.splits (if (= style :triple)
                          [[[10 10 hw h] state.b]
                           [[(+ hw 10) 10 hw (- hh 10)] second]
                           [[(+ hw 10) hh hw hh] second]]
                          (= style :horizontal)
                          [[[10 10 (- hw 10) h] state.b]
                           [[(+ hw 10) 10 hw h] second]]
                          (= style :vertical)
                          [[[10 10 (- w 10) (- hh 10)] state.b]
                           [[10 (- hh 10) w h] second]]))))

(fn reload []
  (each [_ b (pairs (editor.buffer-names))]
    (editor.cmd.save nil b))
  ((. (require :polywell.lib.fennel) "dofile") "config/init.fnl")
  (editor.print "Successfully reloaded config."))

(fn save []
  (let [b (if (= state.b.path "minibuffer") (editor.internal.behind) state.b)]
    (when (and b.needs_save (not b.props.no-file))
      (state.fs.write b.path (.. (table.concat b.lines "\n") "\n"))
      (set b.needs_save false))))

(fn revert []
  (let [contents (state.fs.read state.b.path)]
    (when contents
      (set state.b.lines (lume.split contents "\n"))
      (set state.b.point 0)
      (when (> state.b.point_line (# state.b.lines))
        (set state.b.point_line (# state.b.lines)))
      (when (> state.b.mark (# state.b.lines))
        (set state.b.mark (# state.b.lines))))))

;;; movement

;; backward_char
;; backward_word
;; beginning_of_buffer
;; beginning_of_input
;; beginning_of_line
;; end_of_buffer
;; end_of_line
;; forward-char
;; forward_word
;; jump_to_mark
;; next_line
;; prev_line
;; scroll_down
;; scroll_up

(fn go-to-line []
  (editor.read-line "Go to line: " (fn [l] (editor.go-to (tonumber l)))))

;;; edits and clipboard

;; delete_backwards
;; delete_forwards
;; forward_kill_word
;; kill_line
;; kill_region
;; kill_ring_save
;; backward_kill_word
;; newline
;; newline_and_indent
;; mark
;; no_mark
;; system_copy_region
;; system_yank
;; yank
;; yank_pop

;;; search

(fn search [original-dir]
  (local (point point-line) (editor.point))
  (var continue-from point-line)
  (let [lines (editor.get-lines)
        path (editor.current-buffer-name)
        on-change (fn [find-next new-dir]
                    (let [input (editor.get-input)
                          direction (or new-dir original-dir 1)
                          start (if find-next
                                    continue-from
                                    point-line)]
                      (var i start)
                      (var match nil)
                      (while (and (not (= input "")) (not match)
                                  (> i 0) (<= i (# lines)))
                        (set match (utf8.find (. lines i) input))
                        (when match
                          (set continue-from (+ i direction))
                          (editor.go-to i (- match 1) path))
                        (set i (+ i direction)))))
        callback (fn [_ cancel]
                   (when cancel
                     (editor.go-to point-line point)))
        cancel (partial editor.cmd.exit-minibuffer true)]
    (editor.read-line "Search: " callback
                      {:on-change on-change
                       :cancelable? true
                       :bind {"ctrl-g" cancel
                              "ctrl-n" cancel
                              "ctrl-p" cancel
                              "ctrl-f" (partial on-change true)
                              "ctrl-s" (partial on-change true 1)
                              "ctrl-r" (partial on-change true -1)}})))

(fn replace []
  (let [lines (editor.get-lines)
        (point point-line) (editor.point)
        path (editor.current-buffer-name)
        actually-replace (fn [replace i with replacer y]
                           (when (or (= y "") (= y "y")
                                     (= (string.lower y) "yes"))
                             (let [new (string.gsub (. lines i)
                                                    replace with)]
                               (editor.set-line new i path)
                               (replacer with false (+ i 1)))))
        replacer
        (fn replacer [replace with cancel continue-from]
          (if cancel
              (editor.go-to point-line point)
              (do
                (var match-point nil)
                (var i (or continue-from point-line))
                (while (and (not match-point)
                            (< i (# lines)))
                  (let [match-point (utf8.find (. lines i) replace)]
                    (when match-point
                      (editor.go-to i (- match-point 1) path)
                      (editor.read-line "Replace? [Y/n]"
                                        (partial actually-replace
                                                 replace i with replacer)))
                    (set i (+ i 1)))))))]
    (editor.read-line "Replace: "
                      (fn [replace-text]
                        (editor.read-line (.. "Replace " replace-text " with: ")
                                          (partial replacer replace-text)
                                          {:cancelable? true})))))

;;; other

(fn complete []
  (fn add-prefixes [t input prefixes]
    (let [parts (lume.split input ".")]
      (if (and (< 1 (# parts)) (= (type t) :table))
          (let [first (table.remove parts 1)]
            (when (. t first)
              (table.insert prefixes first)
              (add-prefixes (. t first) (table.concat parts ".") prefixes)))
          (let [prefixed []]
            (each [entry (pairs t)]
              (table.insert prefixes entry)
              (table.insert prefixed (table.concat prefixes "."))
              (table.remove prefixes))
            prefixed))))
  (let [(_ point-line) (editor.point)
        line (editor.get-line point-line)
        entered (or (lume.last (lume.array (string.gmatch line
                                                          "[._%a0-9]+")))
                    "")]
    (when (>= (# entered) 1)
      (let [completions (completion.for entered (add-prefixes _G entered []))]
        (if (= (# completions) 1)
            (editor.handlers.textinput (utf8.sub (. completions 1)
                                                 (+ (# entered) 1)) true)
            (> (# completions) 0)
            (let [common (completion.longest-common-prefix completions)]
              (if (= common entered)
                  (editor.echo (table.concat completions " "))
                  (editor.handlers.textinput (utf8.sub common (+ (# entered) 1))
                                             true))))))))
(fn execute []
  (editor.read-line "Command: "
                    (fn [cmd-name]
                      (let [cmd (. editor.cmd cmd-name)]
                        (if cmd
                            (cmd)
                            (editor.print "Command not found: " cmd-name))))
                    {:completer (fn [input]
                                  (completion.for input editor.cmd))}))

(fn replace-input [input]
  (tset state.b.lines (# state.b.lines) (.. state.b.prompt input))
  (set state.b.point_line (# state.b.lines))
  (set state.b.point (# (lume.last state.b.lines))))

(fn history-prev []
  (when (< state.b.input_history_pos (# state.b.input_history))
    (set state.b.input_history_pos (+ state.b.input_history_pos 1))
    (let [n (+ (- (# state.b.input_history) state.b.input_history_pos) 1)]
      (replace-input (or (. state.b.input_history n) "")))))

(fn history-next []
  (if (>= state.b.input_history_pos 0)
      (do (set state.b.input_history_pos (- state.b.input_history_pos 1))
          (let [n (+ (- (# state.b.input_history) state.b.input_history_pos) 1)]
            (replace-input (or (. state.b.input_history n) ""))))
      (do (set state.b.input_history_pos 0)
          (replace-input ""))))

;; undo

{:find-file find-file :close close :search search :replace replace
 :next-buffer next-buffer :prev-buffer (partial next-buffer -1)
 :switch-buffer switch-buffer
 :focus-next focus-next :split split
 :save save :reload reload :revert revert :complete complete
 :go-to-line go-to-line :execute execute
 :toggle-fullscreen frontend.toggle_fullscreen
 :history-prev history-prev :history-next history-next}
