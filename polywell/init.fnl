(local editor (require :polywell.old))
(local state (require :polywell.state))
(local modes (require :polywell.modes))
(local cmd (require :polywell.commands))

(set editor.handlers (require :polywell.handlers))

(each [name f (pairs cmd)]
  (tset editor.cmd name f))

(fn editor.add-mode [mode]
  (each [_ k (ipairs [:map :ctrl :alt :ctrl-alt :props])]
    (when (not (. mode k))
      (tset mode k {})))
  (each [_ pattern (ipairs (or mode.activate-patterns []))]
    (tset state.activate-patterns pattern mode.name))
  (tset modes (assert mode.name "Missing mode name!") mode)
  ;; auto-define a mode-activating command
  (tset editor.cmd (.. mode.name "-mode")
        (partial editor.activate-mode mode.name)))

(editor.add-mode {:name :default})

(each [k (pairs editor.cmd)]
  (when (: k :match "_")
    (print k)))

editor
