(local state (require :polywell.state))
(local utf8 (require :polywell.lib.utf8))
(local lume (require :polywell.lib.lume))

(fn find-common [a b so-far]
  (if (or (= 0 (# a))
          (~= (utf8.sub a 1 1)
              (utf8.sub b 1 1)))
      (or so-far "")
      (find-common (utf8.sub a 2)
                   (utf8.sub b 2)
                   (.. (or so-far "") (utf8.sub a 1 1)))))

(fn longest-common-prefix [strings common i]
  (let [common (or common (. strings 1) "") i (or i 2)
        new-common (find-common common (. strings i))]
    (if (= i (# strings))
        new-common
        (or (not new-common) (= common ""))
        ""
        (longest-common-prefix strings new-common (+ i 1)))))

(fn add-matches [matches input keys]
  (each [_ v (ipairs keys)]
    (when (and (= (type v) "string") (= (utf8.sub v 1 (# input)) input))
      (table.insert matches v)))
  ;; if there's an exact match, it should go first
  (let [i (lume.find matches input)]
    (when i (table.insert matches 1 (table.remove matches i)))))

(fn completions-for [input context]
  (if (= (type context) "table")
      (let [keys (lume.keys context)
            context (if (and (= (# context) 0) (> (# keys) 0))
                        keys context)]
        ;; if we have an array, use as-is
        ;; for a k/v table or proxied table, use keys
        (doto {} (add-matches input context)))
      {}))

{:longest-common-prefix longest-common-prefix
 :for completions-for}
