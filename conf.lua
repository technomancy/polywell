-- love2d configuration
local lume = require("polywell.lib.lume")

love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Polywell", "polywell"
   t.modules.joystick, t.modules.physics = false, false
   t.modules.sound, t.modules.audio = false, false
   t.window.resizable = true
   if(lume.find(arg, "--test") or lume.find(arg, "--fuzz")) then
      t.window, t.modules.window, t.modules.graphics = false, false, false
   end
end
