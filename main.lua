local fennel = require("polywell.lib.fennel")
table.insert(package.loaders, fennel.make_searcher({correlate=true}))
fennel.path = love.filesystem.getSource() .. "/?.fnl;" ..
   love.filesystem.getSource() .. "/?/init.fnl;" .. fennel.path
local fennelview = require("polywell.lib.fennelview")
_G.pp = function(x) print(fennelview(x)) end

local editor = require("polywell")
local lume = require("polywell.lib.lume")

-- hack for running on android where stdout is broken
if not os.getenv("PWD") then
   local out = io.open("/sdcard/lovegame/out", "w")
   _G["print"] = function(...)
      for _, x in ipairs({...}) do out:write(tostring(x)) end out:write("\n")
   end
end

-- sets up handlers for key, mouse, etc
lume.extend(love, editor.handlers)

-- local refresh = function()
--    love.graphics.clear(love.graphics.getBackgroundColor())
--    love.graphics.origin()
--    polywell.draw()
--    love.graphics.present()
-- end
--
-- Use this if you never have any code that needs to run other than reacting
-- to user input. More efficient.
-- love.run = function()
--    love.load()
--    while true do
--       love.event.pump()
--       local needs_refresh = false
--       for name, a,b,c,d,e,f in love.event.poll() do
--          if(type(love[name]) == "function") then
--             love[name](a,b,c,d,e,f)
--             needs_refresh = true
--          elseif(name == "quit") then
--             os.exit()
--          end
--       end
--       for _,c in pairs(polywell.internal.coroutines) do
--          local ok, val = coroutine.resume(c)
--          if(ok and val) then needs_refresh = true
--          elseif(not ok) then print(val) end
--       end
--       for i,c in lume.ripairs(polywell.internal.coroutines) do
--          if(coroutine.status(c) == "dead") then
--             table.remove(polywell.internal.coroutines, i)
--          end
--       end
--       if(needs_refresh) then refresh() end
--       love.timer.sleep(0.05)
--    end
-- end

-- Use this if you want to use love's built-in event loop. More flexible.
love.update, love.draw = editor.handlers.update, editor.draw

love.load = function()
   if(lume.find(arg, "--fuzz")) then
      require("config")
      require("fuzz")
   else
      love.graphics.setFont(love.graphics.newFont("inconsolata.ttf", 14))
      love.keyboard.setTextInput(true)
      love.keyboard.setKeyRepeat(true)
      require("config")
   end
end
