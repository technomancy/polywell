LUA=polywell/*.lua polywell/frontend/*.lua  config/*.lua
FENNEL=polywell/*.fnl config/*.fnl
LOVE ?= love

check:
	luacheck --no-color --globals love -- $(LUA) main.lua

countlua:
	cloc $(LUA)

countfnl:
	cloc $(FENNEL)

count: countlua countfnl

fuzz: ; $(LOVE) $(PWD) --fuzz

ci: check fuzz count
